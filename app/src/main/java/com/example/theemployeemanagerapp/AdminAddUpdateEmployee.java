package com.example.theemployeemanagerapp;

import androidx.appcompat.app.AppCompatActivity;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;

import com.example.theemployeemanagerapp.Database.EmployeeDatabaseHandler;
import com.example.theemployeemanagerapp.Database.EmployeeOperations;
import com.example.theemployeemanagerapp.Model.Employee;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;




public class AdminAddUpdateEmployee extends AppCompatActivity
{

    private static final String EXTRA_EMPLOYEE_ID = "com.example.EId";
    private static final String EXTRA_ADD_UPDATE = "com.example.add_update";

    private RadioGroup radioGroup;
    private RadioButton trueRadioButton, falseRadioButton;

    private EditText firstNameEditText;
    private EditText lastNameEditText;
    private EditText passwordEditText;
    private EditText hireDateEditText;

    private EditText jobTitleEditText;

    private EditText salaryEditText;

    private ImageView calendarImageView;

    private Button addUpdateButton;

    private Employee newEmpInfo;
    private Employee oldEmpInfo;

    private String mode;

    private long employeeID;

    private EmployeeOperations employeeData;

    final Calendar hireCalendar = Calendar.getInstance();

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_add_update_employee);

        newEmpInfo = new Employee();
        oldEmpInfo = new Employee();

        firstNameEditText = (EditText)findViewById(R.id.edit_text_first_name);
        lastNameEditText = (EditText)findViewById(R.id.edit_text_last_name);
        passwordEditText = (EditText)findViewById(R.id.edit_text_employeepassword);
        hireDateEditText = (EditText)findViewById(R.id.edit_text_hire_date);
        jobTitleEditText = (EditText)findViewById(R.id.edit_text_jobtitle);
        salaryEditText = (EditText)findViewById(R.id.edit_text_salary);

        radioGroup = (RadioGroup)findViewById(R.id.radio_working);

        calendarImageView = (ImageView)findViewById(R.id.image_view_date);

        trueRadioButton = (RadioButton)findViewById(R.id.radio_true);
        falseRadioButton = (RadioButton)findViewById(R.id.radio_false);

        addUpdateButton = (Button)findViewById(R.id.button_add_update_employee);

        ArrayAdapter<CharSequence> adapterJobs = ArrayAdapter.createFromResource(this, R.array.JobTitle, R.layout.spinner_item);
        adapterJobs.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        Spinner jobTitles = (Spinner)findViewById(R.id.spinner_jobs);
        jobTitles.setAdapter(adapterJobs);
        jobTitles.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()
        {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id)
            {
                String jobTitlesText = parent.getItemAtPosition(position).toString();
                jobTitleEditText.setText(jobTitlesText);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView)
            {

            }
        });


        ArrayAdapter<CharSequence> adapterSalaries = ArrayAdapter.createFromResource(this, R.array.BaseSalary, R.layout.spinner_item);
        adapterSalaries.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        Spinner baseSalaries = (Spinner)findViewById(R.id.spinner_salary);
        baseSalaries.setAdapter(adapterSalaries);
        baseSalaries.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()
        {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id)
            {
                String baseSalariesText = parent.getItemAtPosition(position).toString();
                salaryEditText.setText(baseSalariesText);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView)
            {

            }
        });


        employeeData = new EmployeeOperations(this);
        employeeData.open();
        newEmpInfo.setIsworking("True");

        mode = getIntent().getStringExtra(EXTRA_ADD_UPDATE);

        if(mode.equals("Update"))
        {
            addUpdateButton.setText("Update Employee");
            employeeID = getIntent().getLongExtra(EXTRA_EMPLOYEE_ID, 0);

            initializeEmployee(employeeID);
        }

        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId)
            {
                if(checkedId == R.id.radio_true)
                {
                    newEmpInfo.setIsworking("True");
                    if(mode.equals("Update"))
                    {
                        oldEmpInfo.setIsworking("True");
                    }
                }

                else if (checkedId == R.id.radio_false)
                {
                    newEmpInfo.setIsworking("False");
                    if(mode.equals("Update"))
                    {
                        oldEmpInfo.setIsworking("False");
                    }
                }
            }
        });

        addUpdateButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                if(mode.equals("Add"))
                {
                    newEmpInfo.setFirstname(firstNameEditText.getText().toString());
                    newEmpInfo.setLastname(lastNameEditText.getText().toString());
                    newEmpInfo.setPassword(passwordEditText.getText().toString());
                    newEmpInfo.setHiredate(hireDateEditText.getText().toString());
                    newEmpInfo.setJob(jobTitleEditText.getText().toString());
                    newEmpInfo.setSalary(salaryEditText.getText().toString());

                    if(firstNameEditText.getText().toString().isEmpty() || lastNameEditText.getText().toString().isEmpty() || passwordEditText.getText().toString().isEmpty() || hireDateEditText.getText().toString().isEmpty() || jobTitleEditText.getText().toString().isEmpty() || salaryEditText.getText().toString().isEmpty())
                    {
                        Toast.makeText(AdminAddUpdateEmployee.this,"Field or Fields cannot be empty !!",Toast.LENGTH_SHORT).show();
                    }
                    else
                    {
                        employeeData.addEmployee(newEmpInfo);
                        Toast.makeText(AdminAddUpdateEmployee.this, "Employee " + newEmpInfo.getFirstname()+" has been added successfully !", Toast.LENGTH_LONG).show();
                        Intent i = new Intent(AdminAddUpdateEmployee.this,AdminActivity.class);
                        startActivity(i);
                    }
                }

                else
                {
                    oldEmpInfo.setFirstname(firstNameEditText.getText().toString());
                    oldEmpInfo.setLastname(lastNameEditText.getText().toString());
                    oldEmpInfo.setPassword(passwordEditText.getText().toString());
                    oldEmpInfo.setHiredate(hireDateEditText.getText().toString());
                    oldEmpInfo.setJob(jobTitleEditText.getText().toString());
                    oldEmpInfo.setSalary(salaryEditText.getText().toString());

                    if(firstNameEditText.getText().toString().isEmpty() || lastNameEditText.getText().toString().isEmpty() || passwordEditText.getText().toString().isEmpty()  || hireDateEditText.getText().toString().isEmpty() || jobTitleEditText.getText().toString().isEmpty() || salaryEditText.getText().toString().isEmpty())
                    {
                        Toast.makeText(AdminAddUpdateEmployee.this,"Field or Fields cannot be empty !!",Toast.LENGTH_SHORT).show();
                    }
                    else
                    {
                        employeeData.updateEmployee(oldEmpInfo);
                        Toast.makeText(AdminAddUpdateEmployee.this,"Employee " + oldEmpInfo.getFirstname() + " has been updated successfully !!" , Toast.LENGTH_LONG).show();
                        Intent i = new Intent(AdminAddUpdateEmployee.this,AdminActivity.class);
                        startActivity(i);
                    }
                }
            }
        });

        final DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener()
        {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth)
            {
                // TODO Auto-generated method stub
                hireCalendar.set(Calendar.YEAR, year);
                hireCalendar.set(Calendar.MONTH, monthOfYear);
                hireCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateLabel();
            }

        };

        calendarImageView.setOnClickListener(new OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                new DatePickerDialog(AdminAddUpdateEmployee.this, date, hireCalendar.get(Calendar.YEAR), hireCalendar.get(Calendar.MONTH),hireCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

    }

    private void updateLabel()
    {
        String myFormat = "MM/dd/yy"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

        hireDateEditText.setText(sdf.format(hireCalendar.getTime()));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        getMenuInflater().inflate(R.menu.menu_return, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {

        switch (item.getItemId())
        {
            case R.id.action_cancel:
                actionCancel();
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed()
    {
        actionCancel();
    }

    private void actionCancel()
    {
        finish();
    }

    private void initializeEmployee(long employeeID)
    {
        oldEmpInfo = employeeData.getEmployee(employeeID);

        firstNameEditText.setText(oldEmpInfo.getFirstname());
        lastNameEditText.setText(oldEmpInfo.getLastname());
        passwordEditText.setText(oldEmpInfo.getPassword());
        hireDateEditText.setText(oldEmpInfo.getHiredate());

        radioGroup.check(oldEmpInfo.getIsworking().equals("True") ? R.id.radio_true : R.id.radio_false);
        jobTitleEditText.setText(oldEmpInfo.getJob());
        salaryEditText.setText(oldEmpInfo.getSalary());
    }
}
