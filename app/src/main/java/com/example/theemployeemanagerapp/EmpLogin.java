package com.example.theemployeemanagerapp;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import android.util.Log;

import androidx.appcompat.app.AppCompatActivity;

import com.example.theemployeemanagerapp.Database.EmployeeDatabaseHandler;


public class EmpLogin extends AppCompatActivity
{
    private static final String TAG = "Employee Exits";

    public EditText idEditText;
    public EditText passEditText;

    public EditText EmpFN_EditTextShow;
    public EditText EmpLN_EditTextShow;
    public EditText EmpHireDateEditTextShow;
    public EditText EmpWorking_EditTextShow;
    public EditText EmpJob_EditTextShow;
    public EditText EmpSalary_EditTextShow;


    public Button LoginEmp;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_emp_login);

        idEditText = (EditText)findViewById(R.id.edit_text_emp_id_login);
        passEditText = (EditText)findViewById(R.id.edit_text_emp_pass_login);

        EmpFN_EditTextShow = (EditText)findViewById(R.id.edit_text_emp_firstname_show);
        EmpLN_EditTextShow = (EditText)findViewById(R.id.edit_text_emp_lastname_show);
        EmpHireDateEditTextShow = (EditText)findViewById(R.id.edit_text_emp_hire_date_show);
        EmpWorking_EditTextShow = (EditText)findViewById(R.id.edit_text_emp_working_show);
        EmpJob_EditTextShow = (EditText)findViewById(R.id.edit_text_emp_job_show);
        EmpSalary_EditTextShow = (EditText)findViewById(R.id.edit_text_emp_salary_show);

        LoginEmp = (Button)findViewById(R.id.button_login_emp);

        LoginEmp.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                if(idEditText.getText().toString().isEmpty() || passEditText.getText().toString().isEmpty())
                {
                    Toast.makeText(EmpLogin.this,"Empty fields",Toast.LENGTH_SHORT).show();
                }
                else
                {
                    if(check_login(idEditText.getText().toString(),passEditText.getText().toString()) == true)
                    {
                        Toast.makeText(EmpLogin.this,"Successful login",Toast.LENGTH_SHORT).show();
                    }
                    else
                    {
                        Toast.makeText(EmpLogin.this,"Wrong Credentials",Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });

    }

    public boolean check_login(String emp_id, String emp_pass)
    {
        SQLiteOpenHelper database = new EmployeeDatabaseHandler(this);
        SQLiteDatabase db = database.getWritableDatabase();

        String select = "SELECT * FROM employees WHERE employeeID ='" + emp_id + "' AND password='" + emp_pass + "'";

        Cursor c = db.rawQuery(select,null);

        if(c.moveToFirst())
        {
            Log.d(TAG,"Employee exists");

            EmpFN_EditTextShow.setText(c.getString(1));
            EmpLN_EditTextShow.setText(c.getString(2));
            EmpHireDateEditTextShow.setText(c.getString(4));
            EmpWorking_EditTextShow.setText(c.getString(5));
            EmpJob_EditTextShow.setText(c.getString(6));
            EmpSalary_EditTextShow.setText(c.getString(7));

            return true;
        }
        if(c!=null)
        {
            c.close();
        }
        db.close();
        return false;
    }
}
